import { Component} from '@angular/core';
import { MatDialog, _closeDialogVia } from '@angular/material/dialog';
import { Task } from 'src/app/Models/task.model';
import { AssigneeService } from 'src/app/Services/assignee.service';
import { PriorityService } from 'src/app/Services/priority.service';
import { TaskService } from 'src/app/Services/task.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {
  assignees$;
  priorities$;
  task;
  constructor(private matDialog: MatDialog, private assigneeService: AssigneeService, private priorityService: PriorityService, private taskService: TaskService){
    this.getAssignees();
    this.getPriorities();
  }

  getAssignees(){
    this.assigneeService.getAssignees().subscribe(data=>{
      this.assignees$ = data;
    });
  }

  getPriorities(){
    this.priorityService.getPriorities().subscribe(data=>{
      this.priorities$ = data;
    });
  }
  checkboxCheck = false;
  checkCheckBoxvalue(event){
    if(event.checked === true){
      this.checkboxCheck = true;
     }else{
      this.checkboxCheck = false;
     }
  }

  addTask(title, description, startDate, endDate, assigneeId, priorityId, isClosed){
    if(title == "" || title == null || title == false || startDate == "" || startDate == false || startDate == null || endDate == "" || endDate == false || endDate == null || assigneeId == 0 || priorityId == 0){
      alert("Please enter all required fields.");
    }else if(title.length < 3 || title.length > 50){
      alert("Titles must be between 3 and 50 characters.");
    }else{
      let startDateUnformatted = startDate.toLocaleDateString().split("/"); // depends on the regional settings of the PC
      startDate = startDateUnformatted[2] + "-" + startDateUnformatted[1] + "-" + startDateUnformatted[0];
      let endDateUnformatted = endDate.toLocaleDateString().split("/");
      endDate = endDateUnformatted[2] + "-" + endDateUnformatted[1] + "-" + endDateUnformatted[0];
      this.task = new Task(title, description, startDate, endDate, assigneeId, priorityId, this.checkboxCheck);
      this.taskService.addTask(new Task(title, description, startDate, endDate, assigneeId, priorityId, this.checkboxCheck));
      this.matDialog.closeAll();
      alert("Task successfully added!");
      window.location.reload();
    }
  }
}
