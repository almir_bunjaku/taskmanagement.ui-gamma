import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Task } from 'src/app/Models/task.model';
import { AssigneeService } from 'src/app/Services/assignee.service';
import { PriorityService } from 'src/app/Services/priority.service';
import { TaskService } from 'src/app/Services/task.service';

@Component({
  selector: 'edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit{
  assignees$;
  priorities$;
  assignment$;
  taskId = 0;
  task;
  constructor(private matDialog: MatDialog, private shared:TaskService,private assigneeService: AssigneeService, private priorityService: PriorityService, private taskService: TaskService){}
  
  ngOnInit(): void {
    this.taskId = this.shared.getTask().assignmentId;
    this.getAssignmentById(this.taskId);
    this.getAssignees();
    this.getPriorities();
  }

  getAssignees(){
    this.assigneeService.getAssignees().subscribe(data=>{
      this.assignees$ = data;
    });
  }

  getPriorities(){
    this.priorityService.getPriorities().subscribe(data=>{
      this.priorities$ = data;
    });
  }

  getAssignmentById(id: number){
    this.taskService.getAssignmentById(id).subscribe(data=>{
      this.assignment$ = data;
    });
  }
  editTask(title, description, startDate, endDate, assigneeId, priorityId, isClosed){
    if(title == "" || title == null || title == false || startDate == "" || startDate == false || startDate == null || endDate == "" || endDate == false || endDate == null || assigneeId == 0 || priorityId == 0){
      alert("Please enter all required fields.");
    }else if(title.length < 3 || title.length > 50){
      alert("Titles must be between 3 and 50 characters.");
    }else{
      if(!startDate.toString().startsWith("2")){
        let startDateUnformatted = startDate.toLocaleDateString().split("/");
        startDate = startDateUnformatted[2] + "-" + startDateUnformatted[1] + "-" + startDateUnformatted[0];
      }
      if(!endDate.toString().startsWith("2")){
        let endDateUnformatted = endDate.toLocaleDateString().split("/");
        endDate = endDateUnformatted[2] + "-" + endDateUnformatted[1] + "-" + endDateUnformatted[0];
      }
      this.task = new Task(title, description, startDate, endDate, assigneeId, priorityId, isClosed.value);
      this.task.assignmentId = this.taskId;
      this.shared.updateTask(this.task).subscribe();
      alert("Task successfully edited!");
      this.matDialog.closeAll();
      window.location.reload();
    }
  }
}
