import { NULL_EXPR } from "@angular/compiler/src/output/output_ast";

export class Task {
    assignmentId: number = 0;
    title: string;
    description: string;
    startDate: string;
    endDate: string;
    assigneeId: number;
    priorityId: number;
    isClosed: boolean

    constructor(title, description, startDate, endDate, assigneeId, priorityId, isClosed){
        this.title=title;
        this.description=description;
        this.startDate=startDate;
        this.endDate=endDate;
        this.assigneeId=assigneeId;
        this.priorityId=priorityId;
        this.isClosed=isClosed;
    }
  }