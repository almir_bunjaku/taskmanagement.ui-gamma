import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
		providedIn: 'root'
})
export class AssigneeService{
		constructor(private http: HttpClient){}

		getAssignees(){
				return this.http.get('https://localhost:44385/api/Assignee');
		}
}