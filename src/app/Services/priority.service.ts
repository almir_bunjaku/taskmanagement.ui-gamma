import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
		providedIn: 'root'
})
export class PriorityService{
		constructor(private http: HttpClient){}

		getPriorities(){
				return this.http.get('https://localhost:44385/api/Priority');
		}
}