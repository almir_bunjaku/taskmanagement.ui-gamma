import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Task } from '../Models/task.model';

@Injectable({
		providedIn: 'root'
})
export class TaskService{
    constructor(private http: HttpClient){}
    baseURL = 'https://localhost:44385/api/Assignment';
    assigneeURL = 'https://localhost:44385/api/Assignee';
    task: Task;
    addTask(task: Task){
        this.http.post(this.baseURL, task).subscribe();
    }

    retrieveTasks(){
       return this.http.get(this.baseURL);
    }
    
    retrieveAsignees(){
        return this.http.get(this.assigneeURL);
    }

    updateTask(task: Task){
        return this.http.put(this.baseURL, task);
    }

    setTask(data: Task){
        this.task = new Task(data.title, data.description, data.startDate, data.endDate, data.assignmentId, data.priorityId, data.isClosed);
        this.task.assignmentId = data.assignmentId;
    }
    getTask(){
        return this.task;
    }

    getAssignmentById(id: number){
        return this.http.get(this.baseURL + "/" + id);
    }

    deleteTask(id : number){
        this.http.delete(this.baseURL + "/" + id).subscribe();
    }
}