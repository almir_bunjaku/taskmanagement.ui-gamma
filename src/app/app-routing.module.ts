import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import {ClosedTasksComponent} from './components/closed-tasks/closed-tasks.component'
import {OpenTasksComponent} from './components/open-tasks/open-tasks.component'

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'closed', component: ClosedTasksComponent},
  { path: 'open', component: OpenTasksComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
