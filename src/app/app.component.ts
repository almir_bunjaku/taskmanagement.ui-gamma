import { Component } from '@angular/core';
import {ThemePalette} from '@angular/material/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './components/dialog/dialog.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-material-tab-router';  
  navLinks: any[];
  activeLinkIndex = -1; 
  constructor(private router: Router, public dialog: MatDialog) {
    
    this.navLinks = [
        {
            label: 'First',
            link: './',
            index: 0
        }, {
            label: 'Second',
            link: './closed',
            index: 1
        }, {
            label: 'Third',
            link: './',
            index: 2
        }, 
    ];
}
openDialog(){
  this.dialog.open(DialogComponent);
}
ngOnInit(): void {
  this.router.navigate(['home']);
  this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
  });
}
}
