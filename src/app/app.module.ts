import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTabsModule} from '@angular/material/tabs';
import { ClosedTasksComponent } from './components/closed-tasks/closed-tasks.component';
import { OpenTasksComponent } from './components/open-tasks/open-tasks.component';
import {HttpClientModule, HttpClient} from '@angular/common/http';  
import { AssigneeService } from './Services/assignee.service';
import { DialogComponent } from './Components/dialog/dialog.component';
import { EditTaskComponent } from './Components/edit-task/edit-task.component';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClosedTasksComponent,
    OpenTasksComponent,
    DialogComponent,
    EditTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatInputModule,
    TextFieldModule,
    MatDatepickerModule,
    MatSelectModule,
    MatTabsModule,
    MatExpansionModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatCheckboxModule
  ],
  providers: [AssigneeService, MatNativeDateModule],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent]
})
export class AppModule { }
