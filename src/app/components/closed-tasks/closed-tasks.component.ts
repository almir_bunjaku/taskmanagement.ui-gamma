import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/Services/task.service';
import { MatDialog } from '@angular/material/dialog';
import { EditTaskComponent } from '../edit-task/edit-task.component';

@Component({
  selector: 'app-closed-tasks',
  templateUrl: './closed-tasks.component.html',
  styleUrls: ['./closed-tasks.component.css']
})
export class ClosedTasksComponent implements OnInit {
  panelOpenState = false;
  constructor(private shared: TaskService, public dialog: MatDialog) { }
  assignment;
  assignee;
  ngOnInit(): void {
      this.shared.retrieveTasks().subscribe(
        data => {  
          this.assignment = data as string [];        
         }       
      );
      this.shared.retrieveAsignees().subscribe(
        data => {  
          this.assignee = data as string [];       
        }    
      ); 
  }

  openEditDialog(assignment){
    this.shared.setTask(assignment);
    this.dialog.open(EditTaskComponent);
  }
  getAssignee(id : number){
    let _assignee = this.assignee.find(x => x.assigneeId == id);
    return _assignee.firstName + " " + _assignee.lastName;
  }

  deleteTask(id : number){
    if(confirm("Are you sure you want to delete that task?")){
      this.shared.deleteTask(id);
      window.location.reload();
    }
  }

}
