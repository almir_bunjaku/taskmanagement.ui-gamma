import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditTaskComponent } from '../edit-task/edit-task.component';
import { TaskService } from 'src/app/Services/task.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  panelOpenState = false;
  constructor(public dialog: MatDialog, private shared: TaskService) { }
  assignment;
  assignee;
  ngOnInit(): void {
    this.shared.retrieveTasks().subscribe(
      data => {  
        this.assignment = data as string [];        
       }       
    );
    this.shared.retrieveAsignees().subscribe(
      data => {  
        this.assignee = data as string [];       
      }    
    );
}
  openEditDialog(assignment){
    this.shared.setTask(assignment);
    this.dialog.open(EditTaskComponent);
  }
 
  getAssignee(id : number){
    let _assignee = this.assignee.find(x => x.assigneeId == id);
    return _assignee.firstName + " " + _assignee.lastName;
  }

  deleteTask(id : number){
    if(confirm("Are you sure you want to delete that task?")){
      this.shared.deleteTask(id);
      window.location.reload();
    }
  }
}



